function getCurrentData() {
    var url = "//www.nib.si/mbp/static/buoy.data/last_data_slo.html";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var html_tmp = document.createElement('html');
            html_tmp.innerHTML = this.response;
            var data = html_tmp.getElementsByTagName("pre").item(0).innerHTML;
            data = data.replace(/(?:\r\n|\r|\n)/g, '<br>');
            var x = window.open();
            x.document.write(data);
            // x.document.body.style.fontSize = "92%";
            x.document.body.style.fontSize = "initial";
            x.document.title = 'Trenutni podatki';
            x.document.close();
        }
    }
    xhttp.open("GET", url, true);
    xhttp.send();
	return false;
}

Date.prototype.toDateInputValue = (function () {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0, 16);
});
function setInitialDate() {
    var now = new Date();
    var now_2d = new Date();
    now_2d.setDate(now_2d.getDate() - 2);
    datetime_start.value = now_2d.toDateInputValue();
    datetime_end.value = now.toDateInputValue();
}

function checkDatetime() {
    var ds = new Date(datetime_start.value);
    var de = new Date(datetime_end.value);
    var diffDays = (de - ds) / 24. / 60. / 60. / 1000.;
    var rcode = 0;
    if (diffDays >= 6.978) {
        alert("Časovni interval mora biti krajši od 7 dni!");
        rcode = 1;
    }
    if (diffDays <= 0) {
        alert("Datum konca intervala mora biti poznejši od datuma začetka!");
        rcode = 1;
    }
    return rcode;
}

function getChart(sel_plot) {
    if (checkDatetime() != 0) return;
    var base_url = "//www.nib.si/mbp/static/buoy.web/gui/scalar";
    var sel_plot_1 = sel_plot.replace(" ", "+");
    var dt_start = datetime_start.value.replace(":", "%3A") + "%3A00";
    var dt_end = datetime_end.value.replace(":", "%3A") + "%3A00";
    var url = "scalar.html" + "?" + "selectPlot=" + sel_plot + "&" + "startDateTime=" + dt_start + "&" + "endDateTime=" + dt_end;
    window.open(url);
}

// function getChart(sel_plot) {
//     if (checkDatetime() != 0) return;
//     var base_url = "http://www.nib.si/mbp/static/buoy.web/gui/scalar";
//     var sel_plot_1 = sel_plot.replace(" ", "+");
//     var dt_start = datetime_start.value.replace(":", "%3A") + "%3A00";
//     var dt_end = datetime_end.value.replace(":", "%3A") + "%3A00";
//     var new_url = base_url + "?" + "selectPlot=" + sel_plot + "&" + "startDateTime=" + dt_start + "&" + "endDateTime=" + dt_end;
//     var xhttp = new XMLHttpRequest();
//     xhttp.onreadystatechange = function () {
//         if (this.readyState == 4 && this.status == 200) {
//             var html_tmp = document.createElement('html');
//             html_tmp.innerHTML = this.response;
//             pic = html_tmp.getElementsByTagName("img").item(0).src;
//             // var iframe = "<iframe width='100%' height='100%' src='" + pic + "'></iframe>"
//             var x = window.open();
//             x.document.open();
//             x.document.write("<html>");
//             x.document.write("<head>");
//             x.document.write("<meta name='viewport' content='width=device-width, minimum-scale=0.1, maximum-scale=0.4'>");
//             x.document.write("<title>" + sel_plot + "</title>");
//             x.document.write("</head>");
//             x.document.write("<body style='margin: 0px;'>");
//             var pic = html_tmp.getElementsByTagName("img").item(0).src;
//             var img = "<img style='-webkit-user-select: none; height=100%;' src='" + pic + "'>";
//             x.document.write("</body>");
//             x.document.write("</html>");
//             x.document.write(img);
//             // x.document.write(iframe);
//             x.document.close();
//         }
//         else if (this.readyState == 2 && this.status == 500) {
//             alert("Podatki za izbrano obdobje niso na razpolago bodisi zaradi nedelovanja same boje ali pa posameznih senzorjev. Prosimo, izberite drugo obdobje.");
//         }
//     };
//     xhttp.open("GET", new_url, true);
//     xhttp.send();
// }

function getChartVec() {
    if (checkDatetime() != 0) return;
    var base_url = "//www.nib.si/mbp/static/buoy.web/gui/vector";
    var dt_start = datetime_start.value.replace(":", "%3A") + "%3A00";
    var dt_end = datetime_end.value.replace(":", "%3A") + "%3A00";
    var heights = "&selectHeights=2&selectHeights=5&selectHeights=10&selectHeights=15&selectHeights=20";
    var url = "vector.html" + "?" + "&" + "startDateTime=" + dt_start + "&" + "endDateTime=" + dt_end + heights;
    window.open(url);
}

// function getChartVec() {
//     // http://www.nib.si/mbp/static/buoy.web/gui/vector?startDateTime=2020-07-10T11%3A47%3A41&endDateTime=2020-07-12T11%3A47%3A41&selectHeights=2&selectHeights=5&selectHeights=10&selectHeights=15&selectHeights=20
//     var base_url = "http://www.nib.si/mbp/static/buoy.web/gui/vector";
//     var dt_start = datetime_start.value.replace(":", "%3A") + "%3A00";
//     var dt_end = datetime_end.value.replace(":", "%3A") + "%3A00";
//     var heights = "&selectHeights=2&selectHeights=5&selectHeights=10&selectHeights=15&selectHeights=20";
//     var new_url = base_url + "?" + "&" + "startDateTime=" + dt_start + "&" + "endDateTime=" + dt_end + heights;
//     var xhttp = new XMLHttpRequest();
//     xhttp.onreadystatechange = function () {
//         if (this.readyState == 4 && this.status == 200) {
//             var html_tmp = document.createElement('html');
//             html_tmp.innerHTML = this.response;
//             var x = window.open();
//             x.document.open();
//             x.document.write("<html>");
//             x.document.write("<head>");
//             x.document.write("<meta name='viewport' content='width=device-width, minimum-scale=0.1'>");
//             x.document.write("<title>Vektorski graf vetra, morskih valov in tokov</title>");
//             x.document.write("</head>");
//             x.document.write("<body style='margin: 0px;'>");
//             var ii;
//             for (ii = 0; ii < 3; ii++) {
//                 let pic = html_tmp.getElementsByTagName("img").item(ii).src;
//                 let img = "<img style='-webkit-user-select: none;' src='" + pic + "'>";
//                 x.document.write(img);
//             }
//             x.document.write("</body>");
//             x.document.write("</html>");
//             x.document.close();
//         }
//         else if (this.readyState == 2 && this.status == 500) {
//             alert("Podatki za izbrano obdobje niso na razpolago bodisi zaradi nedelovanja same boje ali pa posameznih senzorjev. Prosimo, izberite drugo obdobje.");
//         }
//     };
//     xhttp.open("GET", new_url, true);
//     xhttp.send();
// }

function getBuoyCameras() {
    var url = "//www.nib.si/mbp/sl/oceanografski-podatki/buoy-2/buoy-cam-2";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var html_tmp = document.createElement('html');
            html_tmp.innerHTML = this.response;
            var x = window.open();
            x.document.open();
            x.document.write("<html lang='sl'>");
            x.document.write("<head>");
            x.document.write("<meta name='viewport' content='width=device-width, minimum-scale=0.1'>");
            x.document.write("<title>Bojine nadvodne kamere</title>");
            x.document.write("<style>");
            x.document.write("h3 {");
            x.document.write("font-size: 70px;");
            x.document.write("}");
            x.document.write(" ");
            x.document.write("p {");
            x.document.write("font-size: 50px;");
            x.document.write("}");
            x.document.write("</style>");
            x.document.write("</head>");
            x.document.write("<body style='margin: 0px;'>");
            let target = html_tmp.getElementsByClassName("single_content");
            x.document.write(target[0].innerHTML);
            x.document.write("</body>");
            x.document.write("</html>");
            x.document.close();
        }
        else if (this.readyState == 2 && this.status == 500) {
            alert("Nekaj je šlo narobe.");
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}

function getUWCamera() {
    var url = "//www.nib.si/mbp/sl/oceanografski-podatki/buoy-2/buoy-uw-cam-2";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var html_tmp = document.createElement('html');
            html_tmp.innerHTML = this.response;
            var x = window.open();
            x.document.open();
            x.document.write("<html lang='sl'>");
            x.document.write("<head>");
            x.document.write("<meta name='viewport' content='width=device-width, minimum-scale=0.1'>");
            x.document.write("<title>Bojina podvodna kamera</title>");
            x.document.write("<style>");
            x.document.write("h3 {");
            x.document.write("font-size: 50px;");
            x.document.write("}");
            x.document.write(" ");
            x.document.write("p {");
            x.document.write("font-size: 30px;");
            x.document.write("}");
            x.document.write("</style>");
            x.document.write("</head>");
            x.document.write("<body style='margin: 0px;'>");
            let target = html_tmp.getElementsByClassName("single_content");
            x.document.write(target[0].innerHTML);
            x.document.write("</body>");
            x.document.write("</html>");
            x.document.close();
        }
        else if (this.readyState == 2 && this.status == 500) {
            alert("Nekaj je šlo narobe.");
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}
