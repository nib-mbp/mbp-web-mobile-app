var data = {
    datetime: "N/A",
    airTemp: "N/A",
    airHumid: "N/A",
    windSpd: "N/A",
    windDir: "N/A",
    windGust: "N/A",
    beaufort: "N/A",
    seaTempSurf: "N/A",
    seaTempBot: "N/A",
    seaTempBot1: "N/A",
    seaSalSurf: "N/A",
    oxyBot: "N/A",
    presBot: "N/A",
    waveHeight: "N/A",
    waveMaxHeight: "N/A",
    waveDir: "N/A",
    wavePer: "N/A"
}

function parseParams(text) {
    var regex = /[+-]?\d+(\.\d+)?/g;
    var lines = text.split('\n');
    for (var line = 0; line < lines.length; line++) {
        if (lines[line].substring(0, 20) == '<b>Datum in ura:</b>') {
            data.datetime = lines[line];
        }
        if (lines[line].substring(0, 12) === 'Temperatura:') {
            data.airTemp = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 3) === 'Vla') {
            data.airHumid = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 14) === 'Hitrost vetra:') {
            data.windSpd = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 11) === 'Smer vetra:') {
            data.windDir = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 16) === 'Vetrni sunki do:') {
            data.windGust = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 9) === 'Beaufort:') {
            data.beaufort = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 19) === 'Temperatura na povr') {
            data.seaTempSurf = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 29) === 'Temperatura na dnu - senzor 1') {
            data.seaTempBot = lines[line].match(regex).map(function (v) { return parseFloat(v); })[2];
        }
        if (lines[line].substring(0, 29) === 'Temperatura na dnu - senzor 2') {
            data.seaTempBot1 = lines[line].match(regex).map(function (v) { return parseFloat(v); })[2];
        }
        if (lines[line].substring(0, 15) === 'Slanost na povr') {
            data.seaSalSurf = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 15) === 'Koncentracija O') {
            data.oxyBot = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 11) === 'Tlak na dnu') {
            data.presBot = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 3) === 'Zna') {
            data.waveHeight = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 13) === 'Maksimalna vi') {
            data.waveMaxHeight = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 12) === 'Glavna smer:') {
            data.waveDir = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 16) === 'Srednja perioda:') {
            data.wavePer = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
    }
}


function getParseURLData() {
    var url = "//www.nib.si/mbp/static/buoy.data/last_data_slo.html";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            parseParams(this.response);
            processData();
        }
    }
    xhttp.open("GET", url, true);
    xhttp.send();
}

function infoNA(div) {
    var natext = document.createElement("natext");   // Create a <p> element
    natext.innerHTML = "<p style='margin-top: 50px'>Podatek ni na voljo!</p>";  // Insert text
    document.getElementById(div).appendChild(natext); // Append <p> to <div>
}

function populateHTML() {
    document.getElementById('datetime').innerHTML = data.datetime;
    if (data.airTemp != "N/A") {
        thermometer(data.airTemp, "#airtemp");
    } else {
        infoNA("airtemp");
    }
    var airhumid = document.getElementById("airhumid");
    if (data.airHumid != "N/A") {
        airhumid.setAttribute("data-value", data.airHumid);
        airhumid.style.display = "inline";
    } else {
        airhumid.remove();
        infoNA("airhumid-cont");
    }
    var windmean = document.getElementById("windmean");
    if (data.windDir != "N/A") {
        windmean.setAttribute("data-value", data.windDir);
        var wind_speed_max = 15;
        var wind_speed_len_max = 99;
        var wind_speed_len = Math.round(wind_speed_len_max * data.windSpd / wind_speed_max) + 5;
        if (wind_speed_len > 99) {
            wind_speed_len = 99;
        }
        windmean.setAttribute("data-needle-start", wind_speed_len);
        windmean.setAttribute("data-needle-end", wind_speed_len - 1);
        windmean.setAttribute("data-value-text", data.windSpd + " m/s");
        windmean.style.display = "inline";
    } else {
        windmean.remove();
        infoNA("windmean-cont");
    }

    var windgust = document.getElementById("windgust");
    if (data.windDir != "N/A") {
        windgust.setAttribute("data-value", data.windDir);
        let wind_speed_max = 15;
        let wind_speed_len_max = 99;
        let wind_speed_len = Math.round(wind_speed_len_max * data.windGust / wind_speed_max) + 5;
        if (wind_speed_len > 99) {
            wind_speed_len = 99;
        }
        windgust.setAttribute("data-needle-start", wind_speed_len);
        windgust.setAttribute("data-needle-end", wind_speed_len - 1);
        windgust.setAttribute("data-value-text", data.windGust + " m/s");
        windgust.style.display = "inline";
    } else {
        windmean.remove();
        infoNA("windmean-cont");
    }

    if (data.seaTempSurf != "N/A") {
        thermometer(data.seaTempSurf, "#seasurftemp");
    } else {
        infoNA("seasurftemp");
    }
    if (data.seaTempBot != "N/A") {
        thermometer(data.seaTempBot, "#seabottemp");
    } else {
        infoNA("seabottemp");
    }
    if (data.seaTempBot1 != "N/A") {
        thermometer(data.seaTempBot1, "#seabottemp1");
    } else {
        infoNA("seabottemp1");
    }
    var pressure = document.getElementById("pressurebot");
    pressure.setAttribute("data-value", data.presBot);
}

function processData() {
    if (data.datetime != "N/A") {
        populateHTML();
    } else {
        console.log("Data not available!")
    }
}

function currentGraphics() {
    getParseURLData();
}
