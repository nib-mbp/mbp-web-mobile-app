function windBarb(parent,windspeed,winddir,stroke) {
    var was_calm = true
    var last_rotation

    var canvas_size = 120
    var dot_radius = ~~(canvas_size / 50)
    var calm_radius = dot_radius + 5
    var center = ~~(canvas_size / 2)
    var line_length = ~~(canvas_size / 2.5)
    var full_flag_length = ~~(line_length / 5)
    var half_flag_length = ~~(full_flag_length / 2)
    var animation_duration = 250

    var svg = Snap(canvas_size, canvas_size)
        .attr("id", "windsvg")

    // center dot
    svg.circle(center, center, dot_radius)

    var line = svg.line(center, center, center, center - line_length)
    line.attr({
        'stroke-width': 3,
        // stroke: '#000'
        stroke: stroke
    })
    var line_group = svg.group(line)
    line_group.attr({
        opacity: 0
    })

    var calm = svg.circle(center, center, calm_radius)
    calm.attr({
        fill: 'none',
        'stroke-width': 2,
        // stroke: '#000'
        stroke: stroke
    })

    // ***************************************************************************

    // function random(n) {
    //     return ~~(Math.random() * n)
    // }

    function getWind() {
        var dir = winddir;
        var knots = windspeed * 1.94384449;
        return {
            bearing: dir, // degrees
            speed: knots, // knots
            speedms: windspeed // m/s
        }
    }

    // ***************************************************************************

    function tallyBarbs(speed) {
        // round to nearest 5
        var remainder = 5 * Math.round(speed / 5)
        var pennant = 0
        var full = 0
        var half = 0

        if (remainder >= 50) {
            pennant = ~~(remainder / 50)
            remainder -= (pennant * 50)
        }

        if (remainder >= 10) {
            full = ~~(remainder / 10)
            remainder -= (full * 10)
        }

        if (remainder >= 5) {
            half = ~~(remainder / 5)
        }

        return [pennant, full, half]
    }

    function drawFlag(center, flag_start, flag_length) {
        var flag = svg.line(center, flag_start,
            center + flag_length, flag_start - flag_length)

        flag.attr({
            'stroke-width': 3,
            // stroke: '#000'
            stroke: stroke
        })

        return flag
    }

    function drawPennant(center, flag_start, flag_length) {
        var points = [
            center, flag_start,
            center + flag_length, flag_start,
            center, flag_start + flag_length
        ]

        var flag = svg.polygon(points)

        return flag
    }

    var barb
    function drawBarbs(tally) {
        var flag_start = center - line_length
        var flag_length

        if (barb) {
            if (barb.selectAll('line')) barb.selectAll('line').remove()
            if (barb.selectAll('polygon')) barb.selectAll('polygon').remove()
        }

        var flag
        var flags = []

        tally.forEach(function (g_total, g_idx) {
            if (g_idx === 2) {
                flag_length = half_flag_length

                // Special case: if there’s only a half flag, it’s pushed in from the
                // end to avoid confusion over whether it’s a half flag or full flag.
                if (!flags.length) flag_start += full_flag_length
            } else {
                flag_length = full_flag_length
            }

            for (var n = 0; n < g_total; n++) {
                if (g_idx === 0) {
                    flag = drawPennant(center, flag_start, flag_length)
                } else {
                    // Push the first flag in 1px from the end to avoid a minor visual gap
                    if (!flags.length) flag_start += 1

                    flag = drawFlag(center, flag_start, flag_length)
                }

                flags.push(flag)

                if (g_idx === 0) {
                    if (n === (g_total - 1)) {
                        flag_start += (full_flag_length + half_flag_length)
                    } else {
                        flag_start += full_flag_length
                    }
                } else {
                    flag_start += half_flag_length
                }
            }
        })

        if (barb) flags.forEach(function (f) { barb.append(f) })
        else barb = svg.group.apply(svg, flags)
    }

    function drawWind() {
        var wind = getWind()
        var antipode = 180 + wind.bearing
        if (antipode > 360) antipode -= 360

        if (wind.speed > 2) {
            if (was_calm) {

                calm.animate({
                    r: dot_radius
                }, 250)
            }

            var rotation = 'R' + antipode + ' ' + center + ' ' + center

            var barb_tally = tallyBarbs(wind.speed)
            drawBarbs(barb_tally)

            line_group.selectAll('g').remove()
            line_group.append(barb)

            if (last_rotation) line_group.attr({ tranform: last_rotation })

            line_group.animate({
                opacity: 1,
                transform: rotation
            }, animation_duration)
        } else if (!was_calm) {
            line_group.animate({
                opacity: 0
            }, animation_duration)

            calm.animate({
                r: calm_radius
            }, animation_duration)
        }

        was_calm = wind.speed <= 2
        if (rotation) last_rotation = rotation
        // Boris
        var label = svg.text(0, 0, wind.speedms + " m/s");
        label.attr({
            "font-size": 24,
            "stroke": stroke
        })
        if ((wind.bearing >= 0 & wind.bearing <= 90) | (wind.bearing >= 270 & wind.bearing <= 360)) {
            label.attr({
                'x': center - center / 2,
                'y': center - center / 2
            })
        } else {
            label.attr({
                'x': center - center / 2,
                'y': center + center / 2
            })
        }
    }
    drawWind();  // Boris
    var windsvg = document.getElementById("windsvg");
    windsvg.id = parent + "-" + "content";
    document.getElementById(parent).appendChild(windsvg);
}

