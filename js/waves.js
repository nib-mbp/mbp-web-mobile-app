function wavesSide(elemPlace, elemCanv, waveHeight, waveMaxHeight, waveDir, wavePeriod) {

    var maxHeight = 4 // presumed max wave height is 4 m

    var place = document.getElementById(elemPlace);
    var canvas = document.getElementById(elemCanv);
    canvas.width = place.offsetWidth;
    canvas.height = canvas.width / 2;

    var ctx = canvas.getContext('2d'),
        width = canvas.width,
        height = canvas.height,
        quality = 20;

    var myWave = {
        y: height / 2, // m to pixels
        ampl: waveHeight / 2 * height / maxHeight, // m to pixels 
        length: 9.81 * wavePeriod ** 2 / (2 * Math.PI) * height / maxHeight, // m to pixels
        speed: 1 / wavePeriod  // waves/s
    }

    function stretch(value, inMin, inMax, outMin, outMax) {
        return (value - inMin) * ((outMax - outMin) / (inMax - inMin)) + outMin;
    }

    function wave(x, o, t) {
        var t2 = stretch((t * o.speed) % 1000, 0, 1000, 0, o.length);
        var y1 = Math.sin(stretch(x + (t2 % o.length), 0, o.length, 0, Math.PI * 2));
        return y1 * o.ampl + o.y;
    }

    function drawWave(t) {
        ctx.fillStyle = 'rgb(19, 87, 119, 0.2)';
        ctx.beginPath();
        ctx.moveTo(0, height);
        for (var i = 0; i < width + quality; i += quality) {
            ctx.lineTo(i, wave(i, myWave, t));
        }
        ctx.lineTo(width, height);
        ctx.lineTo(0, height);
        ctx.fill();
    }

    function drawCoordGrid() {
        var grid_size = height / 8;
        var x_axis_distance_grid_lines = 8;
        var y_axis_distance_grid_lines = 0;
        var x_axis_starting_point = { number: 1, suffix: '' };
        var y_axis_starting_point = { number: 1, suffix: '' };
        var num_lines_x = Math.floor(height / grid_size);
        // no of horizontal grid lines
        var num_lines_y = Math.floor(width / grid_size);
        // Draw grid lines along X-axis
        for (var i = 0; i <= num_lines_x; i++) {
            ctx.beginPath();
            ctx.lineWidth = 1;

            // If line represents X-axis draw in different color
            if (i == x_axis_distance_grid_lines)
                ctx.strokeStyle = "#000000";
            else
                ctx.strokeStyle = "#e9e9e9";

            if (i == num_lines_x) {
                ctx.moveTo(0, grid_size * i);
                ctx.lineTo(width, grid_size * i);
            }
            else {
                ctx.moveTo(0, grid_size * i + 0.5);
                ctx.lineTo(width, grid_size * i + 0.5);
            }
            ctx.stroke();
        }
        // Draw grid lines along Y-axis
        for (i = 0; i <= num_lines_y; i++) {
            ctx.beginPath();
            ctx.lineWidth = 1;

            // If line represents Y-axis draw in different color
            if (i == y_axis_distance_grid_lines)
                ctx.strokeStyle = "#000000";
            else
                ctx.strokeStyle = "#e9e9e9";

            if (i == num_lines_y) {
                ctx.moveTo(grid_size * i, 0);
                ctx.lineTo(grid_size * i, height);
            }
            else {
                ctx.moveTo(grid_size * i + 0.5, 0);
                ctx.lineTo(grid_size * i + 0.5, height);
            }
            ctx.stroke();
        }
        // Ticks marks along the positive X-axis
        for (i = 2; i < (num_lines_y - y_axis_distance_grid_lines); i = i + 2) {
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.strokeStyle = "#000000";

            // Draw a tick mark 6px long (-3 to 3)
            ctx.moveTo(grid_size * i + 0.5, height - 5);
            ctx.lineTo(grid_size * i + 0.5, height + 0);
            ctx.stroke();

            // Text value at that point
            ctx.font = '12px Arial';
            ctx.textAlign = 'start';
            ctx.fillStyle = "#000000";
            ctx.fillText(x_axis_starting_point.number * i / 2 + x_axis_starting_point.suffix, grid_size * i - 3, height - 10);
        }
        // Ticks marks along Y-axis
        for (i = 1; i < x_axis_distance_grid_lines; i++) {
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.strokeStyle = "#000000";

            // Draw a tick mark 6px long (-3 to 3)
            ctx.moveTo(0, grid_size * i + 0.5);
            ctx.lineTo(5, grid_size * i + 0.5);
            ctx.stroke();

            // Text value at that point
            ctx.font = '12px Arial';
            ctx.textAlign = 'start';
            ctx.fillStyle = "#000000";
            ctx.fillText(y_axis_starting_point.number * (i - 4) * (-1) / 2 + y_axis_starting_point.suffix, 10, grid_size * i + 3);
        }
        // horizontal line in the middle
        ctx.beginPath();
        ctx.lineWidth = 0.5;
        ctx.strokeStyle = "#000000";
        ctx.moveTo(20, height / 2);
        ctx.lineTo(width, height / 2);
        ctx.stroke();
        // data as text
        ctx.font = '14px Arial';
        ctx.textAlign = 'center';
        ctx.fillStyle = "#000000";
        ctx.fillText('višina: ' + waveHeight + ' (' + waveMaxHeight + ') m' + ', ' + 'smer: ' + waveDir + ' ' + '\u00B0' + ', perioda: ' + wavePeriod + ' s', width / 2, height / 4);
    }


    function draw() {
        var t = new Date().getTime();
        ctx.clearRect(0, 0, width, height);
        drawWave(t);

        drawCoordGrid();

        window.requestAnimationFrame(draw);
    }
    draw();

}




