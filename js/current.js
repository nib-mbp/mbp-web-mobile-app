function drawCurrentData() {
    var datetime = null;
    var airTemp = null;
    var airHumid = null;
    var windSpd = null;
    var windDir = null;
    var windGust = null;
    var beaufort = null;
    var seaTempSurf = null;
    var seaTempBot = null;
    var seaTempBot1 = null;
    var seaSalSurf = null;
    var oxyBot = null;
    var presBot = null;
    var waveHeight = null;
    var waveMaxHeight = null;
    var waveDir = null;
    var wavePer = null;

    var waveMaxHeightNum = null;
    var waveImageIcon = null;

    marginLeft = 20;
    marginRight = 1060;

    getParseURLData();

}

function getParseURLData() {
    var url = "//www.nib.si/mbp/static/buoy.data/last_data_slo.html";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            parseParams(this.response);
            // var canvas = document.getElementById('viewport');
            // context = canvas.getContext('2d');
            // drawFrame(context);
            drawFrame();
        }
    }
    xhttp.open("GET", url, true);
    xhttp.send();
}

function parseParams(text) {
    var regex = /[+-]?\d+(\.\d+)?/g;
    var lines = text.split('\n');
    for (var line = 0; line < lines.length; line++) {
        if (lines[line].substring(0, 20) == '<b>Datum in ura:</b>') {
            datetime = lines[line];
            document.getElementById('datetime').innerHTML = datetime;
        }
        if (lines[line].substring(0, 12) === 'Temperatura:') {
            airTemp = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 3) === 'Vla') {
            airHumid = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 14) === 'Hitrost vetra:') {
            windSpd = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 11) === 'Smer vetra:') {
            windDir = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 16) === 'Vetrni sunki do:') {
            windGust = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 9) === 'Beaufort:') {
            beaufort = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 19) === 'Temperatura na povr') {
            seaTempSurf = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 29) === 'Temperatura na dnu - senzor 1') {
            seaTempBot = lines[line].match(regex).map(function (v) { return parseFloat(v); })[2];
        }
        if (lines[line].substring(0, 29) === 'Temperatura na dnu - senzor 2') {
            seaTempBot1 = lines[line].match(regex).map(function (v) { return parseFloat(v); })[2];
        }
        if (lines[line].substring(0, 15) === 'Slanost na povr') {
            seaSalSurf = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 15) === 'Koncentracija O') {
            oxyBot = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 11) === 'Tlak na dnu') {
            presBot = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 3) === 'Zna') {
            waveHeight = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 13) === 'Maksimalna vi') {
            waveMaxHeight = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
            waveMaxHeightNum = Number(waveMaxHeight);
            if (waveMaxHeightNum <= 0.5) {
                waveImageIcon = 'media/lightSeaOrange.png';
            }
            if (waveMaxHeightNum > 0.5 & waveMaxHeightNum <= 2.5) {
                waveImageIcon = 'media/modSeaOrange.png';
            }
            if (waveMaxHeightNum > 2.5) {
                waveImageIcon = 'media/heavySeaOrange.png';
            }
        }
        if (lines[line].substring(0, 12) === 'Glavna smer:') {
            waveDir = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 16) === 'Srednja perioda:') {
            wavePer = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
    }
}

function drawFrame() {

    var canvas = document.getElementById('viewport');
    context = canvas.getContext('2d');

    context.clearRect(0, 0, canvas.width,
        canvas.height);
    surface_image = new Image();
    surface_image.onload = function () {
        context.drawImage(surface_image, 250, 0, 1250, 1072, 0, 0, 1200, 600);

        context.font = "40pt Calibri";
        var airOut = 'Zrak: ' + Number(airTemp).toFixed(0) + ' °C, ' + Number(airHumid).toFixed(0) + ' %';
        var windOut = 'Veter: ' + Number(windSpd).toFixed(0) + ' m/s (' + Number(windGust).toFixed(0) + ' m/s)';
        var windOut1 = Number(windDir).toFixed(0) + '°  ' + beaufort + ' Bf';
        var waveOut = 'Valovi: ' + waveHeight + ' (' + waveMaxHeight + ') ' + ' m  ' + Number(waveDir).toFixed(0) + '°  ' + wavePer + ' s';

        context.fillStyle = "red";

        context.textAlign = "start";
        context.fillText(airOut, marginLeft, 80);

        context.textAlign = "end";
        context.fillText(windOut, marginRight, 80);
        context.fillText(windOut1, marginRight, 140);

        context.fillStyle = "orange";
        context.textAlign = "center";
        context.fillText(waveOut, 540, 500);
        wave_image = new Image();
        wave_image.src = waveImageIcon;
        wave_image.onload = function () {
            context.drawImage(wave_image, 900, 450, 64, 64);
        }
        wave_image.src = waveImageIcon;
    }
    surface_image.src = '//www.nib.si/mbp/static/buoy.cams/ctrl_img4.jpg?v=1601028001';
    uw_image = new Image();
    uw_image.onload = function () {
        context.drawImage(uw_image, 0, 610, 1250, 942);
        tempBot = seaTempBot + ' °C (' + seaTempBot1 + ' °C)';
        context.font = "40pt Calibri";
        context.fillStyle = "lightgreen";
        context.textAlign = "start";
        context.fillText('Morje (površina):', marginLeft, 700);
        context.fillText('Temp.: ' + seaTempSurf + ' °C', marginLeft, 775);
        context.fillText('Morje (dno):', marginLeft, 1400);
        context.fillText('Temp.: ' + tempBot, marginLeft, 1475);
        context.textAlign = "end";
        context.fillText('Slanost: ' + seaSalSurf, marginRight, 775);
        context.fillText('O' + String.fromCharCode(50 + 8272) + ': ' + oxyBot + ' ml/l', marginRight, 1475);
        context.textAlign = "left";
        context.fillText('Tlak: ' + presBot + ' dBar', 500, 1400);
    }
    uw_image.src = '//www.nib.si/mbp/static/buoy.cams/uw_img.jpg?v=1601040622';
}
