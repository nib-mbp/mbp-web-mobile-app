function getPlotVec() {
    const base_url = "http://www.nib.si/mbp/static/buoy.web/gui/vector";
    const queryString = window.location.search;
    var new_url = base_url + queryString;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var html_tmp = document.createElement('html');
            html_tmp.innerHTML = this.response;
            let picWind = html_tmp.getElementsByTagName("img").item(0).src;
            document.getElementById('plotWind').src = picWind;
            let picWave = html_tmp.getElementsByTagName("img").item(1).src;
            document.getElementById('plotWaves').src = picWave;
            let picCurr = html_tmp.getElementsByTagName("img").item(2).src;
            document.getElementById('plotCurr').src = picCurr;
        }
        else if (this.readyState == 2 && this.status == 500) {
            alert("Podatki za izbrano obdobje niso na razpolago bodisi zaradi nedelovanja same boje ali pa posameznih senzorjev. Prosimo, izberite drugo obdobje.");
        }
    };
    xhttp.open("GET", new_url, true);
    xhttp.send();
}

