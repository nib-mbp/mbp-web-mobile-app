var data = {
    datetime: "N/A",
    airTemp: "N/A",
    airHumid: "N/A",
    windSpd: "N/A",
    windDir: "N/A",
    windGust: "N/A",
    beaufort: "N/A",
    seaTempSurf: "N/A",
    seaTempBot: "N/A",
    seaTempBot1: "N/A",
    seaSalSurf: "N/A",
    oxyBot: "N/A",
    presBot: "N/A",
    waveHeight: "N/A",
    waveMaxHeight: "N/A",
    waveDir: "N/A",
    wavePer: "N/A"
}

function parseParams(text) {
    var regex = /[+-]?\d+(\.\d+)?/g;
    var lines = text.split('\n');
    for (var line = 0; line < lines.length; line++) {
        if (lines[line].substring(0, 20) == '<b>Datum in ura:</b>') {
            data.datetime = lines[line];
        }
        if (lines[line].substring(0, 12) === 'Temperatura:') {
            data.airTemp = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 3) === 'Vla') {
            data.airHumid = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 14) === 'Hitrost vetra:') {
            data.windSpd = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 11) === 'Smer vetra:') {
            data.windDir = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 16) === 'Vetrni sunki do:') {
            data.windGust = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 9) === 'Beaufort:') {
            data.beaufort = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 19) === 'Temperatura na povr') {
            data.seaTempSurf = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 29) === 'Temperatura na dnu - senzor 1') {
            data.seaTempBot = lines[line].match(regex).map(function (v) { return parseFloat(v); })[2];
        }
        if (lines[line].substring(0, 29) === 'Temperatura na dnu - senzor 2') {
            data.seaTempBot1 = lines[line].match(regex).map(function (v) { return parseFloat(v); })[2];
        }
        if (lines[line].substring(0, 15) === 'Slanost na povr') {
            data.seaSalSurf = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 15) === 'Koncentracija O') {
            data.oxyBot = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 11) === 'Tlak na dnu') {
            data.presBot = lines[line].match(regex).map(function (v) { return parseFloat(v); })[1];
        }
        if (lines[line].substring(0, 3) === 'Zna') {
            data.waveHeight = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 13) === 'Maksimalna vi') {
            data.waveMaxHeight = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 12) === 'Glavna smer:') {
            data.waveDir = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
        if (lines[line].substring(0, 16) === 'Srednja perioda:') {
            data.wavePer = lines[line].match(regex).map(function (v) { return parseFloat(v); })[0];
        }
    }
}


function getParseURLData() {
    var url = "//www.nib.si/mbp/static/buoy.data/last_data_slo.html";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            parseParams(this.response);
            processData();
        }
    }
    xhttp.open("GET", url, true);
    xhttp.send();
}

function infoNA(div, width) {

    var svg = d3.select("#" + div)
        .append("svg")
        .attr("width", width)
        .attr("height", width);

    svg.append("text")
        .attr("x", width / 2)
        .attr("y", width / 2)
        .attr("fill", "red")
        .attr("font-size", "14px")
        .attr("font-weight", "bold")
        .attr("text-anchor", "middle")
        .attr("alignment-baseline", "middle")
        .text("Podatek ni na voljo!");

}

function populateHTML() {

    document.getElementById('datetime').innerHTML = data.datetime;

    var width_cont = document.getElementsByClassName('grid-container')[0].clientWidth;
    var width = (width_cont - 5 - 4 * 1) / 2;

    thermometer(data.airTemp, "#airtemp");

    if (typeof data.airHumid !== 'undefined') {
        var airhumid = document.getElementById("airhumid-canv");
        airhumid.setAttribute("data-value", data.airHumid);
        airhumid.style.width = "167px";
        airhumid.style.height = "167px";
    } else {
        airhumid.remove();
        infoNA("airhumid", width);
    }

    if (data.windDir != "N/A") {
        var wind = document.getElementById("wind-canv");
        wind.setAttribute("data-value", data.windDir);
        wind.setAttribute("data-value-text", data.windSpd + " (" + data.windGust + ") m/s");
    } else {
        wind.remove();
        infoNA("wind-canv", width);
    }

    if (data.waveDir != "N/A") {
        var waves = document.getElementById("waves-canv");
        waves.setAttribute("data-value", data.waveDir);
        waves.setAttribute("data-value-text", data.waveHeight + " (" + data.waveMaxHeight + ") m");
    } else {
        waves.remove();
        infoNA("waves-canv", width);
    }

    if (data.waveDir != "N/A") {
        wavesSide("waves-side", "waves-side-canv", data.waveHeight, data.waveMaxHeight, data.waveDir, data.wavePer);
    }

    thermometer(data.seaTempSurf, "#seasurftemp");

    var svg = d3.select("#seasurfsal")
        .append("svg")
        .attr("width", width)
        .attr("height", width);

    svg.append("text")
        .attr("x", width / 2)
        .attr("y", 1 * width / 3)
        .attr("fill", "red")
        .attr("font-size", "28px")
        .attr("font-weight", "bold")
        .attr("text-anchor", "middle")
        .attr("alignment-baseline", "middle")
        .text("Slanost");

    svg.append("text")
        .attr("x", width / 2)
        .attr("y", 2 * width / 3)
        .attr("fill", "red")
        .attr("font-size", "28px")
        .attr("font-weight", "bold")
        .attr("text-anchor", "middle")
        .attr("alignment-baseline", "middle")
        .text(data.seaSalSurf);

    if (typeof data.seaTempBot !== 'undefined') {
        thermometer(data.seaTempBot, "#seabottemp1");
    } else {
        infoNA("seabottemp1", width)
    }

    if (typeof data.seaTempBot1 !== 'undefined') {
        thermometer(data.seaTempBot1, "#seabottemp2");
    } else {
        infoNA("seabottemp2", width)
    }

    if (typeof data.presBot !== "undefined") {
        var pressure = document.createElement('canvas');
        pressure.setAttribute("id", "pressure-canv");
        pressure.setAttribute("data-type", "linear-gauge");
        pressure.setAttribute("data-width", width);
        pressure.setAttribute("data-height", width * 2);
        pressure.setAttribute("data-units", "dBar");
        pressure.setAttribute("data-title", "Pressure");
        pressure.setAttribute("data-min-value", 21);
        pressure.setAttribute("data-max-value", 23);
        pressure.setAttribute("data-major-ticks", "21, 21.5, 22, 22.5, 23");
        pressure.setAttribute("data-value-int", 2);
        pressure.setAttribute("data-value-dec", 1);
        pressure.setAttribute("data-animation-duration", 1500);
        pressure.setAttribute("data-animation-rule", "linear");
        pressure.setAttribute("data-value", data.presBot);
        document.getElementById("pressure").appendChild(pressure);
    } else {
        infoNA("#pressure", width)
    }

    if (typeof data.oxyBot !== "undefined") {

        var svg = d3.select("#oxygen")
            .append("svg")
            .attr("width", width)
            .attr("height", width);

        svg.append("text")
            .attr("id", "text-oxy")
            .attr("x", width / 2)
            .attr("y", 1 * width / 3)
            .attr("fill", "blue")
            .attr("font-size", "28px")
            .attr("font-weight", "bold")
            .attr("text-anchor", "middle")
            .attr("alignment-baseline", "middle")
            .text("Kisik");

        svg.append("text")
            .attr("x", width / 2)
            .attr("y", 2 * width / 3)
            .attr("fill", "blue")
            .attr("font-size", "28px")
            .attr("font-weight", "bold")
            .attr("text-anchor", "middle")
            .attr("alignment-baseline", "middle")
            .text(data.oxyBot + " ml/l");


    } else {
        infoNA("#oxygen", width)
    }

}

function processData() {
    if (data.datetime != "N/A") {
        populateHTML();
    } else {
        console.log("Data not available!")
    }
}

function currentGraphics() {
    getParseURLData();
}
