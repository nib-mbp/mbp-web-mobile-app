function getScalarPlot() {
    const base_url = "//www.nib.si/mbp/static/buoy.web/gui/scalar";
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const selPlot = urlParams.get('selectPlot');
    document.title = selPlot;
    var new_url = base_url + queryString;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var html_tmp = document.createElement('html');
            html_tmp.innerHTML = this.response;
            var pic = html_tmp.getElementsByTagName("img").item(0).src;
            document.getElementById('plot').src = pic;
        }
        else if (this.readyState == 2 && this.status == 500) {
            alert("Podatki za izbrano obdobje niso na razpolago bodisi zaradi nedelovanja same boje ali pa posameznih senzorjev. Prosimo, izberite drugo obdobje.");
        }
    };
    xhttp.open("GET", new_url, true);
    xhttp.send();
}
